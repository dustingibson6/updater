﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Data.SQLite;

namespace Updater
{
    /// <summary>
    /// Interaction logic for NewField.xaml
    /// </summary>
    public partial class NewField : Window
    {
        private Field currentField;
        private int fieldID;
        private bool modified;

        public NewField(String instanceID)
        {
            modified = false;
            InitializeComponent();
            currentField = new Updater.Field(0, "", "", "", "", instanceID, "N");
            SetupField();
        }

        public NewField( int fieldID )
        {
            this.fieldID = fieldID;
            modified = true;
            InitializeComponent();
            SetupField();
        }

        private void SetupField()
        {
            if (modified)
            {
                String sql = "SELECT * FROM FIELD WHERE ID=" + fieldID.ToString();
                SQLiteDataReader reader = DatabaseInstance.Instance.Query(sql);
                while (reader.Read())
                {
                    this.fieldID = DatabaseInstance.Instance.FieldInt(reader, "ID");
                    String fieldName = DatabaseInstance.Instance.FieldString(reader, "NAME");
                    String fieldValue = DatabaseInstance.Instance.FieldString(reader, "VALUE");
                    String fieldURL = DatabaseInstance.Instance.FieldString(reader, "URL");
                    String fieldXpath = DatabaseInstance.Instance.FieldString(reader, "XPATH");
                    String instanceID = DatabaseInstance.Instance.FieldString(reader, "INSTANCE_ID");
                    String isDynamic = DatabaseInstance.Instance.FieldString(reader, "IS_DYNAMIC");
                    modified = true;
                    currentField = new Field(fieldID, fieldName, fieldValue, fieldXpath, fieldURL, instanceID, isDynamic);
                }
                reader.Close();
            }
            this.DataContext = currentField;
        }

        private void testButton_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            xpathValueTextBox.Text = DatabaseInstance.Instance.getXpathValue(currentField.URL, currentField.xpath, currentField.isDynamic);
        }

        private void saveButton_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            String value = DatabaseInstance.Instance.EscapeCharacters(DatabaseInstance.Instance.getXpathValue(currentField.URL, xpathTextBox.Text, currentField.isDynamic));
            String sqlXpath = DatabaseInstance.Instance.EscapeCharacters(currentField.XPath);
            String isDynamicString = "N";
            if (currentField.isDynamic)
                isDynamicString = "Y";
            //int lastRefresh = DatabaseInstance.Instance.ToUnixTimeStamp(DateTime.Now);
            if (value == "")
            {
                MessageBoxResult dialogResult = MessageBox.Show("XPath return nothing. Continue?", "Xpath", MessageBoxButton.YesNo);
                if (dialogResult == MessageBoxResult.No)
                    return;
            }
            String sql = "INSERT INTO FIELD ( XPATH, INSTANCE_ID, URL, NAME, VALUE, IS_DYNAMIC ) VALUES (" +
                "'" + sqlXpath + "','" + currentField.instanceID + "','" + currentField.URL + "','" + currentField.Name + "','" +
                value + "' , '" + isDynamicString + "'" +
                ")";
            if (modified)
            {
                sql = "UPDATE FIELD SET XPATH='" +
                    sqlXpath + "', URL='" + currentField.URL + "',NAME='" + currentField.Name +
                    "',VALUE='" + value + "', IS_DYNAMIC='" + isDynamicString +  "' WHERE ID=" +
                    fieldID.ToString();
            }
            DatabaseInstance.Instance.Execute(sql);
            this.Close();
        }

        private void cancelButton_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }
    }
}
