﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Updater
{
    class InstanceField : INotifyPropertyChanged
    {
        public String instanceID { get; set; }
        public String instanceName { get; set;  }
        public String fieldName { get; set; }
        public String fieldValue { get; set; }
        public String oldValue { get; set;  }
        public String date { get; set;  }
        public int progress;
        public String fieldID;
        public String url;
        public int minutesLeft;
        public int refreshMinutes;

        public int Progress
        {
            get
            {
                double ratio = (double)MinutesLeft / (double)refreshMinutes;
                return (int)((1 - ratio) * 100);
            }
            set
            {
                
            }

        }

        public int RefreshMinutes
        {
            get
            {
                return this.refreshMinutes;
            }
            set
            {
                refreshMinutes = value;
                this.OnPropertyChanged("Progress");
            }
        }

        public int MinutesLeft
        {
            get
            {
                return this.minutesLeft;
            }
            set
            {
                minutesLeft = value;
                this.OnPropertyChanged("Progress");
            }
        }

        public InstanceField(String fieldID, String instanceID, String instanceName, String fieldName, String fieldValue, String oldValue, int refreshMinutes, int minutesLeft, String url, String date="")
        {
            this.fieldID = fieldID;
            this.instanceID = instanceID;
            this.instanceName = instanceName;
            this.fieldName = fieldName;
            this.fieldValue = fieldValue;
            this.oldValue = oldValue;
            this.refreshMinutes = refreshMinutes;
            this.MinutesLeft = minutesLeft;
            double ratio = (double)this.MinutesLeft / (double)this.refreshMinutes;
            this.Progress = (int)((1 - ratio) * 100);
            this.date = date;
            this.url = url;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
