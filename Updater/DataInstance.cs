﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Data.SQLite;
using System.IO;
using System.Net.Http;
using System.Windows.Controls;
using System.Threading;
using HtmlAgilityPack;
using System.Windows.Navigation;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using System.Diagnostics;


namespace Updater
{
    class DatabaseInstance
    {
        private static DatabaseInstance instance = null;
        public SQLiteConnection updaterConnection;
        public int timeDiff;
        private ManualResetEvent messageRecieved;

        public DatabaseInstance()
        {
            timeDiff = 0;
            messageRecieved = new ManualResetEvent(false);
            updaterConnection = new SQLiteConnection("Data Source=updater.db;Version=3;");
            updaterConnection.Open();
            String pragma = "PRAGMA journal_mode = OFF";
            SQLiteCommand command = new SQLiteCommand(pragma, updaterConnection);
            command.ExecuteNonQuery();
            //Check if anything in Global
            if (Int32.Parse(Query("SELECT COUNT(1) FROM GLOBAL")[0].ToString()) < 1)
                Execute("INSERT INTO GLOBAL (LAST_UPDATE) VALUES (" + ToUnixTimeStamp(DateTime.Now).ToString() + ")");
            else
            {
                int timeA = Int32.Parse(Query("SELECT * FROM GLOBAL")[0].ToString());
                timeDiff = (ToUnixTimeStamp(DateTime.Now) - timeA)/60;
                Execute("UPDATE INSTANCE SET MINUTES_LEFT=MINUTES_LEFT-" + timeDiff.ToString());
            }
        }

        public static DatabaseInstance Instance
        {
            get
            {
                if (instance == null)
                    instance = new DatabaseInstance();
                return instance;
            }
        }

        public int ToUnixTimeStamp(DateTime dateTime)
        {
            return (int)(TimeZoneInfo.ConvertTimeToUtc(dateTime) -
                new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)).TotalSeconds;
        }

        public DateTime ToDateTime(int unixTime)
        {
            DateTimeOffset offset = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            return offset.AddSeconds(unixTime).DateTime;
        }

        public SQLiteDataReader Query(String sql)
        {
            SQLiteCommand command = new SQLiteCommand(sql, updaterConnection);
            return command.ExecuteReader();
        }

        public void Execute(String sql)
        {
            SQLiteCommand command = new SQLiteCommand(sql, updaterConnection);
            command.ExecuteNonQuery();
        }

        public int FieldInt(SQLiteDataReader data, String key)
        {
            try
            {
                return data.GetInt32(data.GetOrdinal(key));
            }
            catch (Exception e)
            {
                return -1;
            }
        }

        public String FieldString(SQLiteDataReader data, String key)
        {
            try
            {
                return data.GetString(data.GetOrdinal(key));
            }
            catch (Exception e)
            {
                return "";
            }
        }

        public String EscapeCharacters(String value)
        {
            if (value == null)
                return "";
            String[] escapeStrings = { "'" };
            foreach (String escapeString in escapeStrings)
            {
                value = value.Replace(escapeString, "'" + escapeString);
            }
            return value;
        }

        private String LoadSiteWebClient(String url)
        {
            String html = "";
            using (WebClient client = new WebClient())
            {
                html += client.DownloadString(url);
            }
            return html;
        }


        private String LoadSiteWebRequest(String url)
        {
            WebRequest req = HttpWebRequest.Create("http://google.com");
            req.Method = "GET";
            string source = "";
            using (StreamReader reader = new StreamReader(req.GetResponse().GetResponseStream()))
            {
                source = reader.ReadToEnd();
            }
            return source;
        }

        private String LoadSiteHttpClient(String url)
        {
            String html = "";
            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = client.GetAsync(url).Result)
                {
                    using (HttpContent content = response.Content)
                    {
                        html = content.ReadAsStringAsync().Result;
                    }
                }
            }
            return html;
        }

        private String LoadSiteDynamically(String url)
        {
            var service = PhantomJSDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;
            IWebDriver driver = new PhantomJSDriver(service);
            driver.Url = url;
            driver.Navigate();
            String source = driver.PageSource;
            driver.Close();
            driver.Quit();
            driver.Dispose();
            //System.Windows.Interop.HwndSource hwndSource = System.Windows.Interop.FromHwnd(driver.WindowHandles[0]);
            //Process.GetProcessesByName("PhantomJS")[0].Close();
            return source;
        }

        public String getXpathValue(String url, String xpath, bool isDynamic)
        {
            String html = "";
            try
            {
                if (isDynamic)
                    html = LoadSiteDynamically(url);
                else
                    html = LoadSiteWebClient(url);
                if (html != "")
                {
                    HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
                    htmlDocument.LoadHtml(html);
                    return htmlDocument.DocumentNode.SelectSingleNode(xpath).InnerText;
                }
            }
            catch (Exception e)
            {
                return null;
            }
            return null;
        }

        public String InsertNewFieldHistorySQL()
        {
            return "INSERT INTO VALUE_HISTORY  (VALUE, FIELD_ID, DATE)" +
                " SELECT f.VALUE, f.ID, f.DATE FROM FIELD f" +
                " LEFT JOIN VALUE_HISTORY vh ON f.ID = vh.FIELD_ID" +
                " WHERE vh.FIELD_ID IS NULL;";
        }

        public String FetchInstanceByIDSQL(String instanceID)
        {
            return "SELECT * FROM INSTANCE WHERE ID='" + instanceID + "'";
        }

        public String FetchFieldByInstanceIDSQL(String instanceID)
        {
            return "SELECT * FROM FIELD WHERE INSTANCE_ID='" + instanceID + "' ORDER BY DATE DESC";
        }

        public String UpdateFieldValueByID( String fieldID, String newValueReplace, int refreshDate)
        {
            return "UPDATE FIELD SET VALUE='" + newValueReplace + "', DATE=" + refreshDate.ToString() +
                " WHERE ID=" + fieldID; 
        }

        public String UpdateInstanceTimingSQL(int refreshDate, String instanceID)
        {
            return "UPDATE INSTANCE SET LAST_REFRESH=" + refreshDate.ToString() +
                        ", MINUTES_LEFT = REFRESH_MINUTES " +
                        " WHERE ID='" + instanceID + "'";
        }

        public String InsertHistorySQL(String fieldID, String newValueReplace, int refreshDate)
        {
            return "INSERT INTO VALUE_HISTORY (VALUE, FIELD_ID, DATE) VALUES ('" +
                     newValueReplace + "'," + fieldID + "," + refreshDate.ToString() + ")";
        }

        public String DeleteBlankInstanceSQL()
        {
            return "DELETE FROM INSTANCE WHERE NAME IS NULL";
        }


        public String UpdateMinutesLeftSQL( String instanceID, int minutesLeft)
        {
            return "UPDATE INSTANCE SET MINUTES_LEFT=" + minutesLeft.ToString() +
                       " WHERE ID='" + instanceID + "'"; ;
        }

        public String UpdateLastUpdateSQL(DateTime date)
        {
            return "UPDATE GLOBAL SET LAST_UPDATE=" + ToUnixTimeStamp(date).ToString();
        }

    }
}
