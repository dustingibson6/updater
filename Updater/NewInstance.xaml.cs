﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace Updater
{
    /// <summary>
    /// Interaction logic for NewInstance.xaml
    /// </summary>
    public partial class NewInstance : Window
    {
        private bool modified;
        public String instanceID;
        private UpdateInstance currentInstance;
        private List<Field> fieldList;

        public NewInstance()
        {
            this.instanceID = Guid.NewGuid().ToString();
            String sql = "INSERT INTO INSTANCE (ID) VALUES ('" + instanceID + "')";
            DatabaseInstance.Instance.Execute(sql);
            SetUpInstance();
            currentInstance = new UpdateInstance(instanceID, "", "", 0, 0, 0, -1);
        }

        public NewInstance(String instanceID)
        {
            String sql = "SELECT * FROM INSTANCE WHERE ID='" + instanceID + "'";
            SQLiteDataReader reader = DatabaseInstance.Instance.Query(sql);
            if (reader.Read())
            {
                this.instanceID = instanceID;
                String instanceName = DatabaseInstance.Instance.FieldString(reader, "NAME");
                String instanceURL = DatabaseInstance.Instance.FieldString(reader, "URL");
                String instanceStatus = DatabaseInstance.Instance.FieldString(reader, "STATUS");
                int instanceRefreshMin = DatabaseInstance.Instance.FieldInt(reader, "REFRESH_MINUTES");
                int instanceLastRefresh = DatabaseInstance.Instance.FieldInt(reader, "LAST_REFRESH");
                int instanceLastUpdated = DatabaseInstance.Instance.FieldInt(reader, "LAST_UPDATED");
                int minutesLeft = DatabaseInstance.Instance.FieldInt(reader, "MINUTES_LEFT");
                if (minutesLeft < 1)
                    minutesLeft = instanceRefreshMin;
                currentInstance = new UpdateInstance(instanceID, instanceName, instanceStatus, instanceRefreshMin, instanceLastRefresh, instanceLastUpdated, minutesLeft);
                modified = true;
            }
            SetUpInstance();
        }

        public void SetUpInstance()
        {
            modified = false;
            InitializeComponent();
            UpdateFields();
        }

        private void UpdateFields()
        {
            fieldList = new List<Field>();
            String sql = "SELECT * FROM FIELD WHERE INSTANCE_ID='" + instanceID.ToString() + "'";
            SQLiteDataReader reader = DatabaseInstance.Instance.Query(sql);
            while (reader.Read())
            {
                int fieldID = DatabaseInstance.Instance.FieldInt(reader, "ID");
                String fieldName = DatabaseInstance.Instance.FieldString(reader, "NAME");
                String fieldValue = DatabaseInstance.Instance.FieldString(reader, "VALUE");
                String fieldXpath = DatabaseInstance.Instance.FieldString(reader, "XPATH");
                String fieldURL = DatabaseInstance.Instance.FieldString(reader, "URL");
                String isDynamic = DatabaseInstance.Instance.FieldString(reader, "IS_DYNAMIC");
                //listViewItem.Tag = fieldID;
                fieldList.Add(new Field(fieldID, fieldName, fieldValue, fieldXpath, fieldURL, this.instanceID,isDynamic));
            }
            reader.Close();
            fieldsListView.ItemsSource = fieldList;
            fieldsListView.Items.Refresh();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DataContext = currentInstance;
        }

        private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //String name = nameTextBox.Text;
            //String refreshMinutes = currentInstance.refreshMinutes.ToString();
            //int unixDate = DatabaseInstance.Instance.ToUnixTimeStamp(DateTime.UtcNow);
            //String sql = "update INSTANCE set NAME='" + name + "',LAST_UPDATE=" + unixDate.ToString() +
            //    ",REFRESH_MINUTES=" + refreshMinutes +
            //    " WHERE ID='" + instanceID.ToString() + "'";
            //DatabaseInstance.Instance.Execute(sql);
            //this.DialogResult = true;
            //this.Close();
        }

        private void TextBlock_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            NewField newField = new NewField( this.instanceID );
            newField.ShowDialog();
            UpdateFields();
        }

        private void modifyFieldButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (fieldsListView.SelectedItems.Count > 0)
            {
                int fieldID = ((Field)fieldsListView.SelectedItems[0]).fieldID;
                NewField newField = new NewField( fieldID );
                newField.ShowDialog();
                UpdateFields();
            }
        }

        private void SaveButton_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            String name = nameTextBox.Text;
            String refreshMinutes = currentInstance.refreshMinutes.ToString();
            int unixDate = DatabaseInstance.Instance.ToUnixTimeStamp(DateTime.UtcNow);
            String sql = "update INSTANCE set NAME='" + name + "',LAST_UPDATE=" + unixDate.ToString() +
                ",REFRESH_MINUTES=" + refreshMinutes +
                " WHERE ID='" + instanceID.ToString() + "'";
            DatabaseInstance.Instance.Execute(sql);
            this.DialogResult = true;
            this.Close();
        }

        private void cancelButton1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void Button_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (fieldsListView.SelectedItems.Count > 0)
            {
                String fieldID = ((Field)fieldsListView.SelectedItems[0]).fieldID.ToString();
                String deleteValueHistroy = "DELETE FROM VALUE_HISTORY WHERE FIELD_ID=" + fieldID;
                String sql = "DELETE FROM FIELD WHERE ID=" + fieldID;
                DatabaseInstance.Instance.Execute(sql);
            }
            UpdateFields();
        }
    }
}
