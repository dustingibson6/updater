﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Threading;

namespace Updater
{
    class UpdateInstance : INotifyPropertyChanged
    {
        public String name { get; set; }
        public String instanceId { get; set; }
        public int refreshMinutes;
        public String refreshMinutesString { get; set; }
        public String status { get; set; }
        public DateTime lastUpdated { get; set; }
        public DateTime lastRefresh { get; set; }
        public String minutesLeft { get; set;  }

        public String RefreshMinutes
        {
            get
            {
                if (refreshMinutes <= 0)
                    return "";
                return refreshMinutes.ToString();
            }
            set
            {
                if (value != "")
                {
                    try
                    {
                        refreshMinutes = Int32.Parse(value);
                    }
                    catch
                    {
                        return;
                    }
                }
                else
                {
                    refreshMinutes = 0;
                }
                this.OnPropertyChanged("RefreshMinutes");
                this.OnPropertyChanged("AllowSave");
            }
        }

        public String Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
                this.OnPropertyChanged("Name");
                this.OnPropertyChanged("AllowSave");
            }
        }

        public bool AllowSave
        {
            get
            {
                if (name == "" || name == null || refreshMinutes <= 0)
                    return false;
                else
                    return true;
            }
            set { }
        }

        public String lastUpdatedString
        {
            get
            {
                return lastUpdated.ToString();
            }
            set { }
        }

        public String lastRefreshString
        {
            get
            {
                if (lastRefresh.Year <= 1970)
                    return "--";
                return lastRefresh.ToString();
            }
            set { }
        }

        public UpdateInstance(String instanceId, String name, String status, int refreshMinutes, int lastRefresh, int lastUpdated, int minutesLeft)
        {
            this.instanceId = instanceId;
            this.refreshMinutes = refreshMinutes;
            this.status = status;
            this.name = name;
            this.lastUpdated = DatabaseInstance.Instance.ToDateTime(lastUpdated);
            this.lastRefresh = DatabaseInstance.Instance.ToDateTime(lastRefresh);
            this.minutesLeft = minutesLeft.ToString();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged( String propertyName )
        {
            if( this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
