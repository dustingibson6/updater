﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Updater
{
    class UpdateTimer
    {
        public event EventHandler processEvent;
        public event EventHandler progressEvent;
        public int minutesLeft;
        public int totalMinutes;
        public String state;
        public String instanceID;
        public DispatcherTimer minuteTimer;

        public void CountDown( Object o, EventArgs e )
        {
            if (minutesLeft - 1 > 0)
                minutesLeft -= 1;
            else
                state = "Processing";
            if( state == "Processing")
            {
                //TODO: Process timer
                processEvent(this, e);
                state = "Stopped";
            }
            progressEvent(this, e);
        }

        public UpdateTimer( String instanceID, int totalMinutes, int minutesLeft )
        {
            minuteTimer = new DispatcherTimer();
            minuteTimer.Tag = instanceID;
            this.totalMinutes = totalMinutes;
            this.minutesLeft = minutesLeft;
            minuteTimer.Interval = TimeSpan.FromMinutes(1);
            minuteTimer.Tick += CountDown;
            this.instanceID = instanceID;
            state = "Stopped";
        }

        public void Start()
        {
            minuteTimer.Start();
            state = "Waiting";
        }

        public void Restart()
        {
            if (state == "Waiting")
                minuteTimer.Stop();
            minutesLeft = totalMinutes;
            minuteTimer.Start();
            state = "Waiting";
        }

    }
}
