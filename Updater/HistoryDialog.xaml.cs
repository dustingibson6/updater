﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace Updater
{
    /// <summary>
    /// Interaction logic for HistoryDialog.xaml
    /// </summary>
    public partial class HistoryDialog : Window
    {
        private List<InstanceField> fieldList;
        private InstanceField currentField;

        public HistoryDialog( String fieldID )
        {
            fieldList = new List<InstanceField>();
            //currentField = new InstanceField("","")
            InitializeComponent();
            String fieldSQL = "SELECT FIELD.NAME FIELDNAME, VALUE_HISTORY.VALUE VALUEHISTORY, FIELD.INSTANCE_ID INSTANCEID, VALUE_HISTORY.DATE VALUEDATE, FIELD.URL FIELDURL FROM VALUE_HISTORY " +
                " JOIN FIELD ON FIELD.ID = VALUE_HISTORY.FIELD_ID " +
                " WHERE FIELD_ID=" + fieldID + " ORDER BY VALUEDATE DESC";
            SQLiteDataReader reader = DatabaseInstance.Instance.Query(fieldSQL);
            while( reader.Read())
            {
                String fieldName = DatabaseInstance.Instance.FieldString(reader, "FIELDNAME");
                String fieldValue = DatabaseInstance.Instance.FieldString(reader, "VALUEHISTORY");
                String instanceID = DatabaseInstance.Instance.FieldString(reader, "INSTANCEID");
                int date = DatabaseInstance.Instance.FieldInt(reader, "VALUEDATE");
                String fieldURL = DatabaseInstance.Instance.FieldString(reader, "URL");
                String dateString = DatabaseInstance.Instance.ToDateTime(date).ToString();
                fieldList.Add(new InstanceField(fieldID, instanceID, "", fieldName, fieldValue, "", 0, 0, fieldURL, dateString));
            }
            currentField = fieldList[0];
            historyListBox.ItemsSource = fieldList;
            historyListBox.Items.Refresh();
            this.DataContext = currentField;
        }

        private void historyListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           currentField = (InstanceField)historyListBox.SelectedItem;
            this.DataContext = currentField;
        }
    }
}
