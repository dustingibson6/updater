﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Updater
{
    class Field : INotifyPropertyChanged
    {
        public int fieldID { get; set; }
        public String name { get; set;}
        public String value { get; set; }
        public String xpath { get; set; }
        public String url { get; set; }
        public String instanceID { get; set; }
        public bool isDynamic { get; set; }
        //public String url { get; set; }

        public String Name
        {
            get
            {
                return name;
            }
            set
           {
                this.name = value;
                OnPropertyChanged("Name");
            }
        }

        public String URL
        {
            get
            {
                return url;
            }
            set
            {
                this.url = value;
                OnPropertyChanged("URL");
            }
        }

        public String Value
        {
            get
            {
                return value;
            }
            set
            {
                this.value = value;
                OnPropertyChanged("Value");
            }
        }

        public String XPath
        {
            get
            {
                return xpath;
            }
            set
            {
                this.xpath = value;
                OnPropertyChanged("XPath");
            }
        }

        public bool IsDynamic
        {
            get
            {
                return isDynamic;
            }
            set
            {
                this.isDynamic = value;
                OnPropertyChanged("IsDynamic");
            }
        }

        public Field( int fieldID, String name, String value, String xpath, String URL, String instanceID, String isDynamic)
        {
            this.fieldID = fieldID;
            this.name = name;
            this.value = value;
            this.xpath = xpath;
            this.URL = URL;
            this.instanceID = instanceID;
            if (isDynamic == "Y")
                this.isDynamic = true;
            else
                this.isDynamic = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
