﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Data.SQLite;
using System.Web.UI;

namespace Updater
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        UpdateInstanceFactory updateInstanceFactory;
        volatile Dictionary<String,UpdateTimer> refreshTimers;

        public MainWindow()
        {
            InitializeComponent();
            refreshTimers = new Dictionary<String, UpdateTimer>();
            String deleteSQL = DatabaseInstance.Instance.DeleteBlankInstanceSQL();
            DatabaseInstance.Instance.Execute(deleteSQL);
            updateInstanceFactory = new UpdateInstanceFactory();
            RefreshUI();
        }

        private void RefreshUI()
        {
            updateInstanceFactory.RefreshInstance();
            //instanceListView.Items.Clear();
            List<UpdateInstance> updateInstances = new List<UpdateInstance>();
            RefreshListView();
            foreach (UpdateInstance currentInstance in updateInstanceFactory.getList().Values)
            {
                if (!refreshTimers.ContainsKey(currentInstance.instanceId))
                {
                    UpdateTimer timer = new UpdateTimer( currentInstance.instanceId, currentInstance.refreshMinutes, Int32.Parse(currentInstance.minutesLeft));
                    timer.processEvent += RefreshTimerUpdate;
                    timer.progressEvent += UpdateTimerProgress;
                    timer.Start();
                    refreshTimers.Add(currentInstance.instanceId, timer);
                }
            }
            Application.Current.Dispatcher.Invoke(
                new Action(() =>
                {
                    instanceListView.Items.Refresh();
                }
            ));
        }

        private void RefreshInstance(String instanceID)
        {
            Task refreshTask = new Task(() => RefreshValues(instanceID));
            refreshTask.Start();
        }

        private void RefreshTimerUpdate( Object o, EventArgs e)
        {
            UpdateTimer timer = (UpdateTimer)o;
            RefreshInstance(timer.instanceID);
            //timer.Start();
        }

        private void UpdateTimerProgress( Object o, EventArgs e)
        {
            UpdateTimer timer = (UpdateTimer)o;
            if (timer.minutesLeft > 0)
            {
                var instanceFields = from inst in updateInstanceFactory.getInstanceFieldList()
                                     where inst.Value.instanceID == timer.instanceID
                                     select inst.Value;
                foreach (var instanceField in instanceFields.ToList())
                {
                    instanceField.MinutesLeft = timer.minutesLeft;
                    String sql = DatabaseInstance.Instance.UpdateMinutesLeftSQL(instanceField.instanceID, instanceField.minutesLeft);
                    DatabaseInstance.Instance.Execute(sql);
                }
            }
            String last_update = DatabaseInstance.Instance.UpdateLastUpdateSQL(DateTime.Now);
            DatabaseInstance.Instance.Execute(last_update);
        }

        private void UpdateFields( String instanceID, String newValue )
        {

        }

        private void UpdateStatusLabel(String text)
        {
            Application.Current.Dispatcher.Invoke(
                    new Action(() =>
                    {
                        String label = text;
        statusViewText.Text = label;
                        statusEditText.Text = label;

                    }
                    ));
        }


        private void RefreshValues( String instanceID )
        {
            updateInstanceFactory.CleanFields(instanceID);
            String valueSQL = DatabaseInstance.Instance.InsertNewFieldHistorySQL();
            DatabaseInstance.Instance.Execute(valueSQL);
            String instanceSQL = DatabaseInstance.Instance.FetchInstanceByIDSQL(instanceID);
            int refreshMinutes = 1;
            int minutesLeft = 1;
            SQLiteDataReader instanceReader = DatabaseInstance.Instance.Query(instanceSQL);
            String instanceName = "";
            while(instanceReader.Read())
            {
                refreshMinutes = DatabaseInstance.Instance.FieldInt(instanceReader, "REFRESH_MINUTES");
                instanceName = DatabaseInstance.Instance.FieldString(instanceReader, "NAME");
                minutesLeft = DatabaseInstance.Instance.FieldInt(instanceReader, "MINUTES_LEFT");
            }
            instanceReader.Close();
            String fetchSQL = DatabaseInstance.Instance.FetchFieldByInstanceIDSQL(instanceID);
            SQLiteDataReader reader = DatabaseInstance.Instance.Query(fetchSQL);
            Dictionary<String, String> fields = new Dictionary<String, String>();
            while (reader.Read())
            {
                String url = DatabaseInstance.Instance.FieldString(reader, "URL");
                String fieldID = DatabaseInstance.Instance.FieldInt(reader, "ID").ToString();
                String xpath = DatabaseInstance.Instance.FieldString(reader, "XPATH");
                String oldValue = DatabaseInstance.Instance.FieldString(reader, "VALUE");
                String isDynamicString = DatabaseInstance.Instance.FieldString(reader, "IS_DYNAMIC");
                String date = DatabaseInstance.Instance.FieldInt(reader, "DATE").ToString();
                bool isDynamic = false;
                if (isDynamicString == "Y")
                    isDynamic = true;
                UpdateStatusLabel("Retrieving data from " + url);
                String newValue = DatabaseInstance.Instance.getXpathValue(url, xpath, isDynamic);
                fields.Add(fieldID, fieldID);
                if( ! updateInstanceFactory.getInstanceFieldList().ContainsKey(fieldID) )
                {
                    updateInstanceFactory.AddField(instanceID, instanceName, refreshMinutes, minutesLeft);
                }
                if (newValue != null && newValue != "" && newValue != oldValue)
                {
                    int refreshDate = DatabaseInstance.Instance.ToUnixTimeStamp(DateTime.Now);
                    String newValueReplace = DatabaseInstance.Instance.EscapeCharacters(newValue);
                    String updateInstanceSQL = DatabaseInstance.Instance.UpdateInstanceTimingSQL(refreshDate, instanceID);
                    String updateFieldSQL = DatabaseInstance.Instance.UpdateFieldValueByID(fieldID, newValueReplace, refreshDate );
                    String insertHistory = DatabaseInstance.Instance.InsertHistorySQL(fieldID, newValueReplace, refreshDate);
                    DatabaseInstance.Instance.Execute(updateInstanceSQL);
                    DatabaseInstance.Instance.Execute(updateFieldSQL);
                    DatabaseInstance.Instance.Execute(insertHistory);
                    updateInstanceFactory.getInstanceField(fieldID).oldValue = oldValue;
                    updateInstanceFactory.getInstanceField(fieldID).fieldValue = newValue;
                }
                updateInstanceFactory.getInstanceField(fieldID).refreshMinutes = refreshMinutes;
                updateInstanceFactory.getInstanceField(fieldID).minutesLeft = refreshMinutes;
            }

            //RefreshUI();
            RefreshListView();
            refreshTimers[instanceID].Restart();
            UpdateStatusLabel("");
            //instanceListView.Items.Refresh();
        }

        private void RefreshListView()
        {
            Application.Current.Dispatcher.Invoke(
                new Action(() =>
                {
                    //List<InstanceField> instanceList = updateInstanceFactory.getInstanceFieldList().Select(inst => inst.Value).ToList();
                    instanceListView.ItemsSource = updateInstanceFactory.getList().Select(inst => inst.Value).OrderByDescending( inst => inst.lastUpdated).ToList();
                    viewInstanceListView.ItemsSource = updateInstanceFactory.getInstanceFieldList().Select(inst => inst.Value).OrderByDescending( inst => inst.date).ToList();
                    instanceListView.Items.Refresh();
                }));
        }

        private void listView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }


        private void modifyInstanceButton_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (instanceListView.SelectedItems.Count > 0)
            {
                String instanceID = ((UpdateInstance)instanceListView.SelectedItems[0]).instanceId;
                NewInstance newInstance = new NewInstance(instanceID);
                newInstance.ShowDialog();
                if (newInstance.DialogResult.Value == true)
                {
                    RefreshInstance(instanceID);
                    int date = DatabaseInstance.Instance.ToUnixTimeStamp(DateTime.Now);
                    String updateSQL = DatabaseInstance.Instance.UpdateInstanceTimingSQL(date, instanceID);
                    DatabaseInstance.Instance.Execute(updateSQL);
                }
            }
        }

        private void addInstanceButton1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            NewInstance newInstance = new NewInstance();
            newInstance.ShowDialog();
            if( newInstance.DialogResult == true )
            {
                updateInstanceFactory.AddInstance(newInstance.instanceID);
                UpdateInstance currentInstance = updateInstanceFactory.getList()[newInstance.instanceID];
                UpdateTimer timer = new UpdateTimer(newInstance.instanceID, currentInstance.refreshMinutes, Int32.Parse(currentInstance.minutesLeft));
                timer.processEvent += RefreshTimerUpdate;
                timer.progressEvent += UpdateTimerProgress;
                timer.Start();
                refreshTimers.Add(currentInstance.instanceId, timer);
                RefreshListView();
            }
            //TODO: Add to dictionary
        }

        private void refreshButton_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if( instanceListView.SelectedItems.Count > 0)
            {
                String selectedInstanceID = ((UpdateInstance)instanceListView.SelectedItems[0]).instanceId;
                Task refreshTask = new Task( () => RefreshValues(selectedInstanceID));
                refreshTask.Start();
            }
        }

        private void removeInstanceButton_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (instanceListView.SelectedItems.Count > 0)
            {
                UpdateInstance selectedInstance = (UpdateInstance)instanceListView.SelectedItems[0];
                MessageBoxResult dialogResult = MessageBox.Show("Are you sure you want to remove?", "Delete", MessageBoxButton.YesNo);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    String fieldHistorySQL = "delete from value_history where field_id in (select id from field where INSTANCE_ID='" + selectedInstance.instanceId + "')";
                    String fieldSQL = "delete from field where INSTANCE_ID='" + selectedInstance.instanceId + "'";
                    String sql = "delete from instance where ID='" + selectedInstance.instanceId + "'";
                    DatabaseInstance.Instance.Execute(fieldHistorySQL);
                    DatabaseInstance.Instance.Execute(fieldSQL);
                    DatabaseInstance.Instance.Execute(sql);
                    refreshTimers.Remove(selectedInstance.instanceId);
                    updateInstanceFactory.RemoveInstance(selectedInstance.instanceId);
                    RefreshListView();
                    //updateInstanceFactory.RemoveInstance();
                    //RefreshUI();
                }
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            //foreach (var currentInstance in updateInstanceFactory.getList().Values)
            //{
            //    if( refreshTimers.ContainsKey(currentInstance.instanceId))
            //    {
            //        UpdateTimer refreshTimer = refreshTimers[currentInstance.instanceId];
            //        String sql = "UPDATE INSTANCE SET MINUTES_LEFT=" + refreshTimer.minutesLeft.ToString() +
            //            " WHERE ID='" +  currentInstance.instanceId + "'";
            //        DatabaseInstance.Instance.Execute(sql);
            //    }
            //}
        }

        private void refreshContext_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if(viewInstanceListView.SelectedItems.Count > 0)
            {
                String instanceID = ((InstanceField)viewInstanceListView.SelectedItems[0]).instanceID;
                RefreshInstance(instanceID);
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (viewInstanceListView.SelectedItems.Count > 0)
            {
                String fieldID = ((InstanceField)viewInstanceListView.SelectedItems[0]).fieldID;
                HistoryDialog historyDialog = new HistoryDialog(fieldID);
                historyDialog.ShowDialog();
            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            if(viewInstanceListView.SelectedItems.Count > 0)
            {
                String url = ((InstanceField)viewInstanceListView.SelectedItems[0]).url;
                System.Diagnostics.Process.Start(url);
            }
        }
    }
}
